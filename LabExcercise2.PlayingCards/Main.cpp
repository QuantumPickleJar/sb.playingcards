#include <iostream>
#include <conio.h>

using namespace std;

enum Rank
{
	Two = 2,
	Three = 3,
	Four = 4,
	Five = 5,
	Six = 6,
	Seven = 7,
	Eight = 8,
	Nine = 9,
	Ten = 10,
	Jack = 11,
	Queen = 12,
	King = 13,
	Ace = 14
};

enum Suit
{
	Hearts,
	Spades,
	Clubs,
	Diamonds
};

struct Card
{
	Suit suit;
	Rank rank;
};

/// <summary>Prints information about the passed card</summary> 
/// <param name="card">Card to be printed</param>
void PrintCard(Card card) {

	switch (card.rank)
	{
	case 2: cout << "two"; break;
	case 3: cout << "three"; break;
	case 4: cout << "four"; break;
	case 5: cout << "five"; break;
	case 6: cout << "fix"; break;
	case 7: cout << "seven"; break;
	case 8: cout << "eight"; break;
	case 9: cout << "nine"; break;
	case 10: cout << "ten"; break;
	case 11: cout << "jack"; break;
	case 12: cout << "queen"; break;
	case 13: cout << "king"; break;
	case 14: cout << "ace"; break;
	default: break;
	}
	cout << " of ";

	switch (card.suit)
	{
	case Hearts: cout << "hearts.\n"; break;
	case Clubs: cout << "clubs.\n"; break;
	case Spades: cout << "spades.\n"; break;
	case Diamonds: cout << "diamonds.\n"; break;
	default: break;
	}
}

///<summary>Compare two cards to see which is of greater value</summary>
///<param>First card to compare</param>
///<param>Second card to compare</param>
///<returns>Card of greatest value between the two</returns>
Card HighCard(Card card1, Card card2) {
	//if (card1.rank > card2.rank) { PrintCard(card1) + "Is the higher card"; }

	if (card1.rank > card2.rank) { return card1; }
	else if (card1.rank < card2.rank) { return card2; }
	else {
		cout << "Both cards are equal.  Returning first card...";
		return card1;
	}
	//note for future reference: the ideal way to do this would be to use a ref method similar to the 
	//division method on the calculator project by testing if one IS greater than the other, and passing
	//referenced card into the actual checker method in case it is.
//TODO: 
}


int main() 
{
	//instantiation
	Card c1,c2;

	//define cards 
	c1.rank = Ten;
	c1.suit = Hearts;

	c2.rank = Jack;
	c2.suit = Spades;

	//sample PrintCard()
	PrintCard(c1);
	PrintCard(c2);

	//sample HighCard()
	cout << "Checking which card is higher:\n";
	cout << "\tThe higher card is "; PrintCard(HighCard(c1, c2));

	_getch();
	return 0;
}